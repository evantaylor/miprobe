# v.1 Lab API for pushing data to DynamoDB
# Author Evan Taylor - Evan@EvanTaylor.Pro
import boto3
from boto3.dynamodb.conditions import Key, Attr
import flask
import requests
import simplejson as json
import datetime
from decimal import Decimal
from datetime import datetime 
import ast

app = flask.Flask(__name__)

# Must add AWS ID and keys
aws_id = ''
aws_key = ''
region = 'us-west-2'

dynamodb = boto3.resource('dynamodb', region_name=region, aws_access_key_id=aws_id, aws_secret_access_key=aws_key)
dynamodb.meta.client.meta.config.connect_timeout = 5

# Rename these database table names to match your Dynamo Table Names
Data_DB = "Data_DB" # Sensor Data is stored here
ExperimentID_DB = "Metadata_DB" # Information Metadata is stored here.

def upload_data(data_dict, table_name):
    data_dict = json.loads(json.dumps(data_dict, indent=4, sort_keys=True, use_decimal=True), parse_float=Decimal)
    table = dynamodb.Table(table_name)
    table.put_item(Item=data_dict)


def query_table(table_name, filter_key=None, filter_value=None):
    table = dynamodb.Table(table_name)
    if filter_key and filter_value:
        filtering_exp = Key(filter_key).eq(filter_value)
        response = table.query(KeyConditionExpression=filtering_exp)
    items = response['Items']
    return items

def create_experiment(data_dict):
    config = data_dict['Config']
    upload_data(config, ExperimentID_DB)   


def update_experiment(data_dict):
    upload_data(data_dict, ExperimentID_DB)


def log_error(error_dict):
    experiment = query_table(ExperimentID_DB, filter_key="ExperimentName", filter_value=error_dict['ExperimentName'])
    experiment['Errors'].append(error_dict)
    update_experiment(experiment)


app = flask.Flask(__name__)

@app.route('/echo', methods = ['TRACE','POST'])
def echo_payload():
    dataPayload = flask.request.data
    print(dataPayload)
    response = flask.Response(response=dataPayload, status=200, mimetype='application/json')
    return response


@app.route('/data', methods = ['TRACE','POST'])
def data():
    dataPayload = flask.request.json
    if flask.request.method == 'POST':
        upload_data(dataPayload['Data'], Data_DB)
        update_experiment(dataPayload['Settings'])
    response = flask.Response(response=json.dumps(dataPayload, indent=4, sort_keys=True), status=200, mimetype='application/json')
    return response

@app.route('/error', methods = ['TRACE','POST'])
def error():
    dataPayload = flask.request.json
    log_error(dataPayload)
    response = flask.Response(response={"Message:": "Error Logged."}, status=200, mimetype='application/json')
    return response    


# The experiment metadata is a terrible hack to provide a way of improving real-time dashboarding of various dataloggers running automated experiments.
# Seriously, just delete this code and don't bother with this.  It is a remnant of an earlier time when the author didn't know what he was doing.
@app.route('/metadata/<ExperimentName>', methods= ['TRACE', 'GET'])
def metadata(ExperimentName):
    experiment = str(ast.literal_eval(ExperimentName))
    items = query_table(ExperimentID_DB, filter_key="ExperimentName", filter_value=experiment)[0]

    #Confirm Experiment is Up
    if items['Board'] != "B49":
        interval = int(items['LoggingInterval'])
        reading = int(items['LastReading'])
        readingsTotal = int(items['NumberReadings'])
        sdt = int(items['StartDatetime'])
        dtnow = int(datetime.now().strftime("%s")) * 1000
        thresholdMin = int(((dtnow - sdt) / 1000 / interval))
        alert = reading - thresholdMin
        items['AlertTarget'] = 1
        if reading >= readingsTotal:
            items['AlertValue'] = 1
        else:
            items['AlertValue'] = alert

    data = {}
    data['columns'] = []
    for i in items:
        if i in ['ReadingsLog', 'ErrorLog', 'ExperimentName', 'Metadata', 'Board', 'Timezone', 'Hostname']:
            entry = {
                "name": str(i),
                "type": "text",
                "friendly_name": str(i)
            }
            data['columns'].append(entry)
        elif i in ["StartTimestamp", "LastReadingTime"]:
            entry = {
                "name": str(i),
                "type": "date",
                "friendly_name": str(i)
            }
            data['columns'].append(entry)
        elif i in ['LastReading', 'LoggingInterval', 'NumberReadings', 'StartDatetime', 'AlertValue', 'AlertTarget']:
            entry = {
                "name": str(i),
                "type": "integer",
                "friendly_name": str(i)
            }
            data['columns'].append(entry)
        else:
            entry = {
                "name": str(i),
                "type": "text",
                "friendly_name": str(i)
            }
            data['columns'].append(entry)
    rows = []
    row = {}
    for i in items:
        row[str(i)] = items[i]
    rows.append(row)
    data['rows'] = rows
    response = flask.Response(response=json.dumps(data, indent=4, sort_keys=True), status=200, mimetype='application/json')
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0')
